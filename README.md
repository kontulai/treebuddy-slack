# Treebuddy slack integration

This small node.js app allows you to gift trees in slack.

https://api.slack.com/start/overview#creating

You need to configure the `Request URL` on the Interactivity & Shortcuts section of the app configuration to point to where the app is running, e.g. `https://bdc4a3352b2c.ngrok.io/slack/actions`. It's mandatory to append the `/slack/actions` behind the address.

In the same view, you also need to create a shortcut with a Callback ID `plant_tree`

The app needs `chat:write` and `commands` scopes.

Running the app expects that there's a `.env` file containing at least the following lines:
1. `SLACK_SIGNING_SECRET=` which you get from your app's basic information page, and 
2. `SLACK_ACCESS_TOKEN=` which you get from your app's OAuth & Permissions page.

