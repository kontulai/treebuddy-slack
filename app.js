require('dotenv').config()
const { createMessageAdapter } = require('@slack/interactive-messages');
const slackSigningSecret = process.env.SLACK_SIGNING_SECRET;
const slackInteractions = createMessageAdapter(slackSigningSecret);
const { WebClient } = require('@slack/web-api');
const token = process.env.SLACK_ACCESS_TOKEN;

const web = new WebClient(token);

const port = process.env.PORT || 3000;


slackInteractions.shortcut('plant_tree', (payload) => {
       // This example shortcut opens a view (needs to complete under 2.5 seconds)
       // You can use https://app.slack.com/block-kit-builder/ to build views.
       return web.views.open({
         token: token,
         trigger_id: payload.trigger_id,
         view: {
              "type": "modal",
              "callback_id": "plant_tree_modal",
              "title": {
                     "type": "plain_text",
                     "text": "Give a tree gift",
                     "emoji": true
              },
              "submit": {
                     "type": "plain_text",
                     "text": "Submit",
                     "emoji": true
              },
              "close": {
                     "type": "plain_text",
                     "text": "Cancel",
                     "emoji": true
              },
              "blocks": [
                     {
                            "type": "section",
                            "block_id": "selector",
                            "text": {
                                   "type": "mrkdwn",
                                   "text": "Select a user who you want to give the gift to"
                     },
                     "accessory": {
                            "type": "users_select",
                                   "placeholder": {
                                          "type": "plain_text",
                                          "text": "Select a user",
                                          "emoji": true
                                   },
                                   "action_id": "user"
                            }
                     }
              ]
       }
       })
     });

     slackInteractions.viewSubmission('plant_tree_modal', (payload) => {
       console.log(payload.view.state);
     
       // The previous value is an object keyed by block_id, which contains objects keyed by action_id,
       // which contains value properties that contain the input data. Let's log one specific value.
       console.log(payload.view.state.values.selector.user.selected_user);
     
       // Validate the inputs (errors is of the shape in https://api.slack.com/surfaces/modals/using#displaying_errors)
       // Not really sure how validation actually works, so skipping it like a boss. ;)
       //const errors = validate(payload.view.state);
     
       // Return validation errors if there were errors in the inputs
       //if (errors) {
       //  return errors;
       //}
       (async () => {
              // Post a message to the user, and await the result.
              const result = await web.chat.postMessage({
                text: `Here's your tree gift! Claim it here https://treebuddy.earth/trees/claim/TESTI`,
                channel: payload.view.state.values.selector.user.selected_user,
              });
            
              // The result contains an identifier for the message, `ts`.
              console.log(`Successfully send message ${result.ts} to ${payload.view.state.values.selector.user.selected_user}`);
            })();
     });




(async () => {
       const server = await slackInteractions.start(port);
       console.log(`Listening for events on ${server.address().port}`);
})();
